using System;
using Xunit;

namespace XUnitTestProject1
{
	public class UnitTest1
	{
		[Fact]
		public void TestSuccess1()
		{

		}


		[Fact]
		public void TestSuccess2()
		{

		}

		[Theory]
		[InlineData("https://www.google.com")]
		[InlineData("https://www.google.co.uk")]
		[InlineData("https://www.google.co.ukjfff")]
		[InlineData("https://www.google.co.ukjf")]
		[InlineData("https://www.google.co.uhh")]
		public void TestMalformedUri(string uri)
		{
			new Uri(uri);
		}
	}
}
